;;; talash.el --- Emacs fronted for talash -*- lexical-binding: t; -*-
;;
;; Created: April 02, 2023
;; Modified: April 02, 2023
;; Version: 0.0.1
;; Keywords: search
;; Homepage: https://codeberg.org/rahguzar/talash
;; Package-Requires: ((emacs "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:

(require 'external-completion)

(defun talash--parse-match ()
  "Parse the match at point and return the candidate string."
  (let ((gapp t))
    (when (looking-at (rx eol))
      (cl-callf not gapp)
      (delete-line))
    (while (not (looking-at (rx eol)))
      (unless gapp
        (put-text-property (point) (pos-eol) 'face 'completions-common-part))
      (goto-char (pos-eol))
      (delete-char 1)
      (cl-callf not gapp))
    (delete-and-extract-region (pos-bol) (pos-eol))))

(defun talash--parse-matches (str)
  "Parse matches for STR and return them as a list."
  (let ((matches nil))
    (goto-char (point-min))
    (while (not (search-forward (concat str "\n\n\n\n") nil 'move))
      (sit-for 0.005 t)
      (goto-char (point-min)))
    (delete-region (point-min) (point))
    (while (not (and (> (point-max) 4)
                     (string-equal
                      "\n\n\n\n" (buffer-substring-no-properties
                                  (- (point-max) 4) (point-max)))))
      (sit-for 0.005 t))
    (delete-region (- (point-max) 3) (point-max))
    (while (not (eobp))
      (push (talash--parse-match) matches)
      (delete-line))
    (nreverse matches)))

(defun talash--std-process (args)
  "Start a talash process with ARGS. Return the process."
  (make-process :name "talash"
                :buffer (generate-new-buffer "talash")
                :command `("talash" "piped" "raw" ,@args)
                :noquery t
                :connection-type 'pipe))

(defun talash-completion-table (proc)
  "A completion table for talash process PROC."
  (external-completion-table
   'talash
   (lambda (pat _)
     (with-current-buffer (process-buffer proc)
       (erase-buffer)
       (process-send-string proc (concat pat "\n"))
       (let ((matches (while-no-input (talash--parse-matches pat))))
         (unless (booleanp matches)
           matches))))
   `((display-sort-function . identity))))

(defun talash (&optional prompt &rest args)
  "Choose a string using talash based on ARGS.
PROMPT is as in `completing-read'."
  (let ((proc (talash--std-process args))
        (numc nil))
    (accept-process-output proc)
    (with-current-buffer (process-buffer proc)
      (goto-char (point-min))
      (setq numc (delete-and-extract-region (point) (pos-eol))))
    (unwind-protect
        (completing-read
         (concat "(" numc ") " (or prompt "Talash: "))
         (talash-completion-table proc))
      (process-send-eof proc)
      (kill-buffer (process-buffer proc)))))

(provide 'talash)
;;; talash.el ends here
